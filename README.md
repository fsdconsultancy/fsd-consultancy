All Fire Safety Measures that are listed within a property Annual Fire Safety Statement must be assessed by a Competent Fire Safety Practitioner, against the minimum standard of performance as per the Essential Fire Safety Measures.

Address: 4/1307 Botany Road, Suite 2, Mascot, NSW 2020, Australia

Phone: +612 9669 4032

Website: https://firesystemdesign.com.au
